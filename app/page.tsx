"use client"
import Link from "next/link";
import Banner from "./component/Banner";
import Image from "next/image";
import Navbar from "./component/Navbar";
import '../css/index.css';
import { useState } from "react";

interface typeData {
  title: string;
  content: JSX.Element | string;
  link: string;
  file: string
}

export default function Home() {
  const [datalist, setDatalist] = useState([
    {
      title: 'Guidance',
      content: (
        <div>
          <p className=" mb-4">
            Material.io supports this design kit with documentation andguidance for how to use the components and styles
          </p>
          <p>
            Our specifications have been updated for Material You toprovide for additional guidance for modern styling, adaptiveguidance, and color.
          </p>
        </div>
      ),
      link: 'Material.io',
      file: 'https://images.pexels.com/photos/23715908/pexels-photo-23715908.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load'
    },
    {
      title: 'Typography',
      content: (
        <div>
          <p className=" mb-4">
            Google Fonts makes it easy to bring personality andperformance to your websites and products. Our catalog ofopen-source fonts makes it easy to integrate expressive typeand icons seamlessly-no matter where you are in the world.
          </p>
          <p>
            Our font catalog places typography front and center, invitingyou to explore, sort, and test fonts for use in more than 135languages.
          </p>
        </div>
      ),
      link: 'fonts.google.com',
      file: 'https://images.pexels.com/photos/13576343/pexels-photo-13576343.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load'
    },
    {
      title: 'lcons',
      content: (
        <div>
          <p className="mb-4">          For the official Material Design icons, use our stickersheet,managed by Google Fonts team. Add it to your Figma teamaccount, publish it, and start using it's components as anicon library or embed icons in your component library foreasyicon swapping. Material lcons are available in five stylesand a range of downloadable sizes and densities. The iconsare crafted based on the core design principles and metricsof Material Design guidelines.
          </p>
          <p>          The icon stickersheet is regularly maintained and updates arepublished bi-weekly.
          </p>
        </div>
      ),
      link: 'figma.com/@googlefonts',
      file: 'https://images.pexels.com/photos/26587929/pexels-photo-26587929.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load'
    },
    {
      title: 'Colors',
      content: (
        <div>
          <p className="mb-4">
            Color is used to express style and communicate meaning.With dynamic color, Material puts personal color preferencesand individual needs at the forefront of systematic colorapplication.
          </p>
          <p>
            Material You reimagines color as a more individualizedexperience with dynamic and accessible color expression
          </p>
        </div>
      ),
      link: 'Material You Figma Plugin',
      file: 'https://images.pexels.com/photos/26582960/pexels-photo-26582960.jpeg?auto=compress&cs=tinysrgb&w=600&lazy=load'
    }
  ]);

  return (
    <main className="min-h-screen">
      <Navbar/>
      <Banner />
      <div className="w-full bg-white">
        <div className=" max-w-[75rem] mx-auto flex flex-col gap-14 mobile:gap-16 py-16 px-4">
          {
            datalist.map(item => {
              return (
                <div
                  key={item.title}
                  className="flex mobile:flex-col-reverse gap-8 mobile:gap-4 items-center li-item cursor-pointer"
                >
                  <div className="flex-1">
                    <h1 className=" text-2xl font-bold mb-3 title">{item.title}</h1>
                    <div className="text-base mb-2">{item.content}</div>
                    <div>
                      <Link
                        href={''}
                        className=" text-base text-[#6750a4] font-bold"
                      >
                        <ins>{item.link}</ins>
                      </Link>
                    </div>
                  </div>
                  <div className="flex-1 mobile:w-full">
                    <div className="relative aspect-[600/250] w-full rounded-3xl overflow-hidden">
                      <Image
                        alt=""
                        fill
                        src={item.file}
                        className="image"
                        style={{ 'objectFit': 'cover' }}
                      />
                    </div>
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    </main>
  );
}
