import Image from "next/image";

const Banner = () => {


  return (
    <div className="relative aspect-[1920/800] w-full">
      <Image
        alt=""
        fill
        style={{ 'objectFit': 'cover' }}
        src={'https://images.pexels.com/photos/26236827/pexels-photo-26236827.jpeg'}
      />
      <div className=" absolute w-full h-full top-0 left-0 z-10 flex flex-col justify-center">
        <div className="max-w-[75rem] mx-auto w-full px-4">
          <h1 className=" text-[2.5rem] mobile:text-[2rem] font-bold mb-3 title">Guidance</h1>
          <div className="text-xl mb-2 xl:max-w-[42%] md:max-w-[60%]">
          Material You reimagines color as a more individualizedexperience with dynamic and accessible color expression
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;