const Navbar = () => {

    return (
        <div className="bg-white sticky top-0 h-16 w-full z-[999] border-b border-[#ddd]">
            <div className="max-w-[75rem] mx-auto flex justify-between items-center h-[100%] px-4">
                <div className="text-3xl font-extrabold">LOGO</div>
                <div className="text-base">user-卢熠航</div>
            </div>
        </div>
    );
};

export default Navbar;